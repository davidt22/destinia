<?php

require_once 'classes/Db.php';
require_once 'classes/Lodging.php';
require_once 'classes/Apartment.php';
require_once 'classes/Hotel.php';

use classes\Lodging;

function read_stdin()
{
    $fr = fopen("php://stdin","r");
    $input = fgets($fr, 3);
    $input = rtrim($input);
    fclose($fr);

    return $input;
}

echo "Input only 3 characters to search into our Hotels and Apartments: " . PHP_EOL;

$search = read_stdin();

$db = new \classes\Db();
$lodgings = $db->select("SELECT * FROM lodging WHERE name LIKE '%$search%' OR type LIKE '%$search%' ORDER BY name ASC");

if(count($lodgings) == 0){
    echo "There are no results for this search. Try with another name or type(Hotel or Apartamento).";
    exit;
}

$results = array();
foreach($lodgings as $lodging){
    if($lodging['type'] === Lodging::HOTEL){
        $hotel = $db->select("SELECT * FROM hotel WHERE lodging_id = ".$lodging['id']);
        $lodgingFinal = new \classes\Hotel($lodging['name'], $hotel[0]['stars'], \classes\Hotel::convertStandardRoomType($hotel[0]['standard_room_type']), $lodging['city'], $lodging['country']);
    }elseif($lodging['type'] === Lodging::APARTMENT){
        $apartment = $db->select("SELECT * FROM apartment WHERE lodging_id = ".$lodging['id']);
        $lodgingFinal = new \classes\Apartment($lodging['name'], $apartment[0]['avaliable_apartments'], $apartment[0]['capacity'], $lodging['city'], $lodging['country']);
    }

    echo utf8_encode($lodgingFinal->__toString()).PHP_EOL;
}