<?php


namespace classes;


class Hotel extends Lodging
{
    const SINGLE = 'Habitación sencilla';
    const DOUBLE = 'Habitación doble';
    const DOUBLE_PLUS = 'Habitación doble con vistas';

    /** @var integer */
    protected $stars;

    /** @var string */
    protected $standardRoomType;

    function __construct($name, $stars, $standardRoom, $city, $country)
    {
        parent::__construct($name, Lodging::HOTEL, $city, $country);
        $this->stars = $stars;
        $this->standardRoomType = $standardRoom;
    }

    /**
     * @return int
     */
    public function getStars()
    {
        return $this->stars;
    }

    /**
     * @param int $stars
     */
    public function setStars($stars)
    {
        $this->stars = $stars;
    }

    /**
     * @return string
     */
    public function getStandardRoomType()
    {
        return $this->standardRoomType;
    }

    /**
     * @param string $standardRoomType
     */
    public function setStandardRoomType($standardRoomType)
    {
        $this->standardRoomType = $standardRoomType;
    }

    public static function convertStandardRoomType($roomType)
    {
        $type = '';
        switch($roomType){
            case 'SINGLE':
                $type = self::SINGLE;
                break;
            case 'DOUBLE':
                $type = self::DOUBLE;
                break;
            case 'DOUBLE_PLUS':
                $type = self::DOUBLE_PLUS;
                break;
        }

        return utf8_decode($type);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name.', '.$this->stars.' estrellas, '.$this->standardRoomType.', '.$this->city.', '.$this->country;
    }
}