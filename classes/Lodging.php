<?php


namespace classes;


abstract class Lodging
{
    const HOTEL= 'Hotel';
    const APARTMENT = 'Apartment';

    /** @var integer $id */
    protected $id;

    /** @var string $name */
    protected $name;

    /** @var string $type */
    protected $type;

    /** @var string $city */
    protected $city;

    /** @var string $country */
    protected $country;


    public function __construct($name, $type, $city, $country)
    {
        $this->name = $name;
        $this->type = $type;
        $this->city = $city;
        $this->country = $country;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
}