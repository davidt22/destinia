<?php


namespace classes;


class Apartment extends Lodging
{
    const SINGLE = 'Habitación sencilla';
    const DOUBLE = 'Habitación doble';
    const DOUBLE_PLUS = 'Habitación doble con vistas';

    /** @var integer */
    protected $avaliableApartments;

    /** @var integer */
    protected $capacity;

    function __construct($name, $avaliableApartments, $capacity, $city, $country)
    {
        parent::__construct($name, Lodging::HOTEL, $city, $country);
        $this->avaliableApartments = $avaliableApartments;
        $this->capacity = $capacity;
    }

    /**
     * @return int
     */
    public function getAvaliableApartments()
    {
        return $this->avaliableApartments;
    }

    /**
     * @param int $avaliableApartments
     */
    public function setAvaliableApartments($avaliableApartments)
    {
        $this->avaliableApartments = $avaliableApartments;
    }

    /**
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @param int $capacity
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name.', '.$this->avaliableApartments.' apartamentos, '.$this->capacity.' adultos, '.$this->city.', '.$this->country;
    }
}